import React from 'react';
import Logo from '../../Assets/Image/Logo.svg'
import {ReactComponent as LogoIcon} from '../../Assets/Image/Logo.svg'
import './header.css'

const Header = () => {
    return (
        <div className="flex header">
            <div>
                <LogoIcon/>
                {/* <img src={Logo} alt="Logo" /> */}
            </div>
            <div className='flex'>
                <li>
                    <a href="#Home">Home</a>
                </li>
                <li>
                    <a href="#Products">Products</a>
                </li>
                <li>
                    <a href="#About">About</a>
                </li>
            </div>
            <div>
                ACTION
            </div>
        </div>
    );
}
 
export default Header;